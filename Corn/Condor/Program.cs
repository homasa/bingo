﻿/**
 * Copyright (C) Homa Asadi, 2021.
 * All rights reserved.
 * Homa Asadi; homa@asadi
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Condor
{
	static public class FileHelper
	{
		static public List<string> GetAll(string path)
		{
			try
			{
				var l = Directory.GetFiles(path);
				foreach (var s in l)
				{
					Console.WriteLine(s);
				}
				return null;
			}
			catch (Exception p)
			{
				Console.WriteLine(p);
				throw;
			}
		}
	}

	static public class DirectoryHelper
	{
		static public List<string> GetAll(string path)
		{
			try
			{
				var l = Directory.GetDirectories(path);
				foreach (var s in l)
				{
					Console.WriteLine(s);
				}
				return null;
			}
			catch (Exception p)
			{
				Console.WriteLine(p);
				throw;
			}
		}
		static public void OperateOnFiles(string path)
		{
			try
			{

			}
			catch (Exception p)
			{
				Console.WriteLine(p);
				throw;
			}
		}

		static public void OperateOnDirectorys(string path)
		{
			try
			{
				var ls = Directory.GetFiles(path);
			}
			catch (Exception p)
			{
				Console.WriteLine(p);
				throw;
			}
		}
	}

	static public class Program
	{
		static public void StopAndCheck()
		{
			var co = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("Continue ?");
			var ky = Console.ReadKey(intercept: true);
			if (ky.KeyChar != '\r')
				Environment.Exit(1);
			Console.ForegroundColor = co;
		}

		static public void procedure1(string path)
		{
			try
			{
				var ds = Directory.GetDirectories(path);
				foreach (var di in ds)
				{
					Console.WriteLine($"Directory Name = {di}");

					//
					// checking if 'Artworks' folder exists
					var dx = new DirectoryInfo(di);
					if (!dx.GetDirectories().Any(o => o.Name.Equals("artworks", StringComparison.OrdinalIgnoreCase)))
					{
						//
						// there is no 'Artworks' folder hence create it
						dx.CreateSubdirectory("Artworks");
					}

					//
					// checking if any images exist in main folder
					var fs = dx.GetFiles("*.jpg");
					if (fs.Length != 0)
					{
						foreach (var fi in fs)
						{
							File.Move(fi.FullName, Path.Combine(dx.FullName, "Artworks") + @"\" + fi.Name);
						}
					}

					StopAndCheck();
				}
			}
			catch (Exception p)
			{
				Console.WriteLine(p);
			}
		}

		static public void Main(string[] args)
		{
			try
			{
				procedure1(@"Y:\Music\Song (Pop, Aeoliah)");
			}
			catch (Exception p)
			{
				Console.WriteLine(p);
			}
		}
	}
}