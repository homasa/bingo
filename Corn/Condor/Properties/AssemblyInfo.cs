﻿/**
 * Copyright (C) Homa Asadi, 2021.
 * All rights reserved.
 * Homa Asadi; homa@asadi
 */

using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Bingo")]
[assembly: AssemblyDescription("Condor Module")]
[assembly: AssemblyConfiguration("Microsoft Windows NT, Microsoft .NET Framework 4.8")]
[assembly: AssemblyCompany("Bingo")]
[assembly: AssemblyProduct("Bingo")]
[assembly: AssemblyCopyright("Copyright © Homa Asadi")]
[assembly: AssemblyTrademark("Bingo")]
[assembly: AssemblyCulture("")]

[assembly: ObfuscateAssemblyAttribute(false)]

[assembly: CLSCompliant(false)]
[assembly: ComVisible(false)]

[assembly: Guid("074db331-9b9d-4a5e-8fbb-702508c267f8")]

[assembly: AssemblyVersion("0.1.0.0")]